/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frog;

/**
 *
 * @author bychkov-sy
 */
public class Frog {

    boolean[] lamps = new boolean[5];

    /**
     *  Prints result of jumping of 5 frogs
     */
    public void run() {
        for (int i = 1; i <= 5; ++i) {
            jump(i);
            System.out.println("State of lamps after frog number " + i + " jumps: " + stateString());
        }
    }

    /**
     *
     * @return String - 1 when lamp is on , 0 when lamp is off
     */
    private String stateString() {
        StringBuilder result = new StringBuilder();
        for (boolean lamp : lamps) {
            result.append(lamp ? "1" : "0").append(" ");
        }
        return result.toString();
    }

    /**
     * Simulates jumping of frog over buttons that switchs the lamps
     *
     * @param i - number of frog
     *
     */
    private void jump(int i) {
        int j = -1;
        while (j + i < 5) {
            j += i;
            lamps[j] = !lamps[j];
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Frog frog = new Frog();
        frog.run();
    }
}
